var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const PORT = process.env.PORT || 3000;
//Подключение статической папки
app.use(express.static(__dirname + '/public'));
//Подключение к сокету
io.on('connection', (socket) => {
    console.log('a user connected');
    //выбор комнаты
    socket.on('select room', (room) => {
        socket.room = room;
        socket.join(room);//Подключить к выбранной  комнате
        
    });
    //отключение
    socket.on('disconnect', () => {
        if(socket.login&&io.sockets.adapter.rooms[socket.room])
        {
            let room = io.sockets.adapter.rooms[socket.room];
            room.length;
            //количество клиентов в комнате
            socket.in(socket.room).broadcast.emit('log out', {username:socket.username, userCount: room.length, roomName:socket.room});
            //in(socket.room)- конкретно из этой комнаты
        }
    });
    //Добавление пользователя
    socket.on('add user', (username) => {
        let room = io.sockets.adapter.rooms[socket.room];
        room.length;
        socket.username = username;
        socket.login = true;
        socket.userId =  room.length;
        socket.emit('login', {username:username,userId: socket.userId, userCount:  room.length, roomName:socket.room});
        socket.in(socket.room).broadcast.emit('add user', {username:username,userId:socket.userId, userCount:  room.length, roomName:socket.room});
    });
    //Отправка сообщения
    socket.on('messege', (messege) => {
        io.in(socket.room).emit('messege', {messege:messege,username:socket.username, userId:socket.userId});
    });
    //Печатает
    socket.on('typing', ()=> {
        io.in(socket.room).emit('typing', {username:socket.username,userId:socket.userId});
    });
    //Перестал печатать
    socket.on('stop typing', ()=> {
        io.in(socket.room).emit('stop typing', {username:socket.username, userId:socket.userId});
    });
    
});
//слушаем сервер на нашем порту
server.listen(PORT, function () {
  console.log('Server listening at port %d', PORT);
});